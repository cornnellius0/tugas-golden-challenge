const express = require('express');
const morgan = require('morgan');

const logger = require('morgan');
const userRouter = require('./routers/users');
const orderRouter = require('./routers/order');

const app = express();

const port = 3000;

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(morgan('combined'))

app.use('/users', userRouter)
app.use('/orders', orderRouter)

app.use((req, res, next) => {
    return next({
        status: 404,
        message: 'Not Found'
    })
})

app.use((err, req, res, next) => {
    res.status(err.status || 500).send(err.message)
})

app.listen(port, () => console.log(`Server listen on ${port}`))