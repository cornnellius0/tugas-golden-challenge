const userHandler = require('../handlers/users');

const userRouter = require('express').Router();

userRouter.post('/register', userHandler.register)
userRouter.post('/logn', userHandler.login)

module.exports = userRouter;