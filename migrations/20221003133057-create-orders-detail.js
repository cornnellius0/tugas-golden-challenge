'use strict';

const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orders_details', {

      order_id: {
        type: Sequelize.INTEGER
      },
      item_id: DataTypes.INTEGER,
      jumlah_pesanan:DataTypes.INTEGER,
      sub_total:DataTypes.NUMERIC,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orders_details');
  }
};