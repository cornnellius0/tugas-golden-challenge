'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class items extends Model {
    static associate(models) {
      // define association here
    }
  }
  items.init({
    nama: DataTypes.STRING,
    kategori: DataTypes.STRING,
    harga: DataTypes.NUMERIC,
    stok: DataTypes.INTEGER,
    deskripsi: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'items',
  });
  return items;
};
