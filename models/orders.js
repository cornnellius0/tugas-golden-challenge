'use strict';
const { types } = require('pg');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  orders.init({
    id_user: DataTypes.INTEGER,
    total_harga:DataTypes.NUMERIC,
    tanggal_pesanan:DataTypes.NUMERIC,
    status_pesanan:DataTypes.NUMERIC,

  }, {
    sequelize,
    modelName: 'orders',
  });
  return orders;
};