const FastestValidator = require('fastest-validator');

const formValidation = new FastestValidator({
    useNewCustomCheckerFunction: true,
    messages: {
        emailHarusDiisi: "Email Tidak Boleh Kosong",
        password: "Password minimal 8 karakter dan harus mengandung setidaknya 1 angka dan 1 huruf kapital",
        NamaLengkapHarusDiisi: "Nama Lengkap Tidak Boleh Kosong"

    }
});

module.exports = function (req, res, next) {
    const schema = {
        email: {
            type: "email",
            custom: (v, errors) => {
                if (v == '') {
                    errors.push({ type: "emailHarusDiisi" })
                    return
                }
                return v;
            },
        },
        nama_lengkap: {
            type: "string",
            custom: (v, errors) => {
                if (v == '') {
                    errors.push({ type: "NamaLengkapHarusDiisi" })
                    return
                }
                return v;
            },
        },
        password: {
            type: "any",
            custom: (v, errors) => {
                if (typeof v === 'number') {
                    errors.push({ message: "tidak boleh password hanya terdiri dari angka", type: "password" })
                    return
                }
                const pattern = /^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/;
                if (pattern.exec(v) == null) {
                    errors.push({ type: "password" });
                }
                return v;
            },
        }
    }
    const isValidate = formValidation.validate(req.body, schema)
    if (isValidate !== true) {
        return res.status(400).send(isValidate)
    } else {
        return next();
    }
}