### Installation Instruction
---
To run the project, please follow the instruction

1. First, run this command for to install the required package

```bash
npm install -g sequelize-cli
npm install 
```

2. Perform migration, please make sure that *PostgreSQL* daemon is active/run
but first, copy the local env to set as default configuration using this command
```bash
cp .env.example .env
```

3. Open `.env` file and set your database config properly, and then run the migration using this command

```bash
NODE_ENV=development sequelize db:create
NODE_ENV=development sequelize db:migrate
NODE_ENV=development sequelize db:seed:all
```

4. After Migration is done, than you can start the application using this command


```bash
#with monitor mode
npm run dev

#without monitor mode --- less resource usage
npm run start
```