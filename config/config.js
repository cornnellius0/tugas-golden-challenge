require('dotenv').config();

const {DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASS,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres",
  },
  test: {
    username: DB_USER,
    password: DB_PASS,
    database: DB_NAME,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres",
  },
  production: {
    username: "postgres",
    password: "binarian1",
    database: "tugas",
    host: "localhost",
    port: 54332,
    dialect: "postgres",
    logging: false,
  },
};