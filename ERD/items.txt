CREATE TABLE `Items` (
  `ID` int,
  `KATEGORI` varchar(255),
  `NAMA` varchar(255),
  `HARGA` numeric,
  `STOCK` int,
  `DESKRIPSI` text,
  PRIMARY KEY (`ID`)
);