const db = require("../models");
const User = db.sequelize.model('users');

const register = async (req, res) => {
    const {email, name, password} = req.body;

    try {
        const existUser = await User.findOne({
            where: { email }
        });
        if(existUser) {
            return res.status(400).send("Gagal mendaftar, email sudah pernah digunakan")

        }
        const userCreated = await User.create({email, name, password})
    
        if(userCreated) {
            return res.status(201).send("User berhasil ditambahkan. UserID: " + userCreated.id)
        }
        
    } catch (error) {
        console.log(error.message)
        return res.status(500).send("Gagal menambahkan user")
    }
}

const login = async (req, res) => {
    const { email, password } = req.body;
    const isFoundUser = await users.findOne({ where: { email } });
  

    if (isFoundUser) {
        const isValidPassword = isFoundUser.password == password;
        const namaLengkap = isFoundUser.namaLengkap;
        if (isValidPassword) {
            const jwtPayload = jwt.sign(
                {
                    id: isFoundUser.id,
                    email: isFoundUser.email,
                    namaLengkap: isFoundUser.namaLengkap
                }, "Cornel123"
            );
            return res.json({
                token: jwtPayload,
                message: "Login Berhasil",
                NamaLengkap: namaLengkap
            });
        }}
}

const userHandler = {
    register, login
}

module.exports = userHandler;