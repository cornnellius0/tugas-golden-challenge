const { sequelize } = require('../models')
const Order = sequelize.model('orders')

const getOrderById = async (req, res) => {
    try {
        const order = await Order.findByPk(req.params.id)
        if(!order) return res.status(400).send("Order tidak ditemukan")
        return res.json(order)
    } catch (error) {
        return res.status(500).send("Gagal mencari data")
    }
}

module.exports = {
    getOrderById,
}